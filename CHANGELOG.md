# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.0.0]() (07-05-2022)

### Features

-   Adding app.yaml file initial configuration ([]())
-   Adding .gitlab-ci.yml initial file ([]())